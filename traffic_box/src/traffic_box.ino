// Rickie Kerndt <rkerndt@cs.uoregon.edu>
//
// Particle Electron acting as a relay to signal phase requests
//
PRODUCT_ID(5046)
PRODUCT_VERSION(5)

#include "tc-relay.h"
#include "tc-loop.h"

#define NUM_RELAYS 4
#define NUM_LOOPS 1

// string constants
const char * const true_str = "true";
const char * const false_str = "false";
const char * const on_str = "on";
const char * const off_str = "off";

// on board led
int led = D7;
int enable_led = 0; // 0=off, 1=on
String led_state = String(off_str);

// relay gpio pins
const int relay_pins[] = { 0, B0, B1, B2, B3 };

// loop detect gpio pins
const int loop_pins[] = { 0, C0 };

// default maximum relay timeout
const unsigned int default_timeout = 120000; // 2 minutes
static unsigned int current_timeout = default_timeout;

// Relays & Loops
Relay* relays[NUM_RELAYS+1];
Loop* loops[NUM_LOOPS+1];

// exponential average constants
double alpha = 0.10;
double beta = 0.90;

// global variables for tracking performance
int total_pings = 0;
int cell_rssi = 0;
double cell_avg_rssi = 0.0;
int cell_quality = 0;
double cell_avg_quality = 0.0;
const char *ready = false_str;
int enable_serial = 0;
String serial_state = String(off_str);
int last_update = Time.now();
const int update_interval = 1;

//enable serial logging
SerialLogHandler logHandler;

void setup() {

  // initialize relays
  for (int i = 1; i <= NUM_RELAYS; i++) {
    relays[i] = new GPIO_Relay(i, relay_pins[i], current_timeout);
  }

  // initialize loop detection
  for (int i = 1; i <= NUM_LOOPS; i++) {
    loops[i] = new GPIO_Loop(i, loop_pins[i]);
  }

  // pinmode for on board led
  pinMode(led, OUTPUT);

  // start serial port so we can monitor
  if (enable_serial) {
    Serial.begin();
  }

  // access control and state variables
  Particle.variable("total_pings", total_pings);
  Particle.variable("cell_rssi", cell_rssi);
  Particle.variable("cell_avg_rss", cell_avg_rssi);
  Particle.variable("cell_quality", cell_quality);
  Particle.variable("cell_avg_qua", cell_avg_quality);
  Particle.variable("serial_state", serial_state);
  Particle.variable("led_state", led_state);

  // control functions
  Particle.function("request_ping", request_ping);
  Particle.function("toggle_seria", toggle_serial);
  Particle.function("toggle_led", toggle_led);
  Particle.function("relay_on", relay_on);
  Particle.function("relay_off", relay_off);
  Particle.function("get_timeout", get_timeout);
  Particle.function("set_timeout", set_timeout);
}

void loop() {
  int mark = Time.now();
  if (mark - last_update > update_interval) {
    last_update = mark;
    update_stats();
    output_stats();
    output_loop_states();
  }
}

int request_ping(String arg) {
  total_pings += 1;
  int id = arg.toInt();
  return id;
}

int toggle_serial(String arg) {
  if (enable_serial == 0) {
    Serial.begin();
    enable_serial = 1;
    serial_state = String(on_str);
  }
  else {
    Serial.end();
    enable_serial = 0;
    serial_state = String(off_str);
  }
  return enable_serial;
}

int toggle_led(String arg) {
  if (enable_led == 0) {
    enable_led = 1;
    led_state = String(on_str);
    digitalWrite(led, HIGH);
  }
  else {
    enable_led = 0;
    led_state = String(off_str);
    digitalWrite(led, LOW);
  }
  return enable_led;
}

int relay_on (String arg) {
  int relay_num = arg.toInt();
  if ((relay_num > 0) && (relay_num <= NUM_RELAYS)) {
    relays[relay_num]->on();
  }
  else {
    relay_num = 0;
  }
  return relay_num;
}

int relay_off (String arg) {
  int relay_num = arg.toInt();
  if ((relay_num > 0) && (relay_num <= NUM_RELAYS)) {
    relays[relay_num]->off();
  }
  else {
    relay_num = 0;
  }
  return relay_num;
}

void update_stats() {
  // set values of operating parameters
  CellularSignal sig = Cellular.RSSI();
  cell_rssi = sig.rssi;
  cell_quality = sig.qual;
  cell_avg_rssi = (cell_rssi * alpha) + (cell_avg_rssi * beta);
  cell_avg_quality = (cell_quality * alpha) + (cell_avg_quality * beta);
  if (Cellular.ready()) {
    ready = true_str;
  }
  else {
    ready = false_str;
  }
}

void output_stats() {
  if (Serial.isConnected()) {
    Serial.printf("ready=%s, rssi=%i, qual=%i\n", ready, cell_rssi, cell_quality);
  }
}

void output_loop_states() {
  for (int i = 1; i <= NUM_LOOPS; i++) {
    if (loops[i]->process() == 1) {
      Particle.publish(loops[i]->get_name(), String(loops[i]->is_on()));
    }
  }
}

int get_timeout(String arg) {
  return (int) current_timeout;
}

int set_timeout(String arg) {
  int value = arg.toInt();
  if (value > 0) {
    for (int i = 1; i <= NUM_RELAYS; ++i) {
      relays[i]->set_timeout(value);
    }
    current_timeout = value;
  } else {
    value = 0;
  }
  return value;
}
