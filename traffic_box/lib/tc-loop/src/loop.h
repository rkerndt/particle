/* Rickie Kerndt <rkerndt@cs.uoregon.edu
   Loop detection
   loop.h */

#pragma once
#include <Particle.h>

static const char * const loop_base_name = "Loop_";

class Loop {
protected:
  int id;    // unique identifier
  String name; // event names have max 64 characters
  int state; // 0= on, 1= off

public:
  Loop(int id);

  virtual int process(void);
  virtual int is_on(void) const;
  virtual int is_off(void) const;
  virtual String get_name(void) const;
};

class GPIO_Loop : public Loop {
  int pin; // actual gpio pin for closure detection

public:
  GPIO_Loop(int id, int pin);

  int process(void);
};
