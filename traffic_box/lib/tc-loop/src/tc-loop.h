/* Rickie Kerndt <rkerndt@cs.uoregon.edu
   Loop detection
   loop.h */

#pragma once
#include <Particle.h>

static const char * const loop_base_name = "Loop_";

/* Abstract class for loop detection. Derived classes implement mechanisms
   for checking loop state */
class Loop {
protected:
  int id;    // unique identifier
  String name; // event names have max 64 characters
  int state; // 0= on, 1= off


public:
  Loop(int id);
  virtual ~Loop(void) {}

  /* Define method in derived classes. Must update state attribute
     as loop on (1) or loop off (0). Returns 1 if state changed, 0
     if no change in state */
  virtual int process(void)=0;

  /* Returns 1 if loop on and 0 if loop off */
  virtual int is_on(void) const;

  /* Returns 0 if loop off and 1 if loop on */
  virtual int is_off(void) const;

  /* Returns string identifer */
  virtual String get_name(void) const;
};

/* Uses gpio pin in DigitalRead mode to sense a contact closure which
   represents loop state */
class GPIO_Loop : public Loop {
  int pin;

public:
  GPIO_Loop(int id, int pin);

  /* updates loop status.
     returns: 1 if status changed, 0 if not changed */
  int process(void);
};
