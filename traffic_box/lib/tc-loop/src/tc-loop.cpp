/* Rickie Kerndt <rkerndt@cs.uoregon.edu
   loop.cpp */

#include "tc-loop.h"

Loop::Loop(int id) {
  this->id = id;
  this->name = String(loop_base_name);
  this->name.concat(String(id));
  this->state = -1;
}

int Loop::is_on(void) const {
  return state == 1;
}

int Loop::is_off(void) const {
  return state == 0;
}

String Loop::get_name(void) const {
  return name;
}

GPIO_Loop::GPIO_Loop(int id, int pin) : Loop(id), pin(pin) {
  pinMode(pin, INPUT_PULLDOWN);
}

int GPIO_Loop::process(void) {
  int value = digitalRead(pin);
  int change = 0;
  if (value == LOW) {
    if (state != 0) {
      state = 0;
      change = 1;
    }
  }
  else if (value == HIGH) {
    if (state != 1) {
      state = 1;
      change = 1;
    }
  }
  return change;
}
