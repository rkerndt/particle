/* relay library by Rickie Kerndt
 */

#include "tc-relay.h"

Relay::Relay(int id) : max_timeout(0), id(id), count(0), state(0) {}

Relay::Relay(int id, unsigned int to) : max_timeout(to), id(id), count(0),
  state(0) {}

unsigned int Relay::get_timeout(void) const {
  return max_timeout;
}

void Relay::set_timeout(unsigned int ms) {
  max_timeout = ms;
}

int Relay::get_count(void) const {
  return count;
}

int Relay::get_state(void) const {
  return state;
}

int Relay::get_id(void) const {
  return id;
}

GPIO_Relay::GPIO_Relay (int id, unsigned int pin) :
  Relay(id), timeout(NULL), pin(pin) {
  setup();
}

GPIO_Relay::GPIO_Relay (int id, unsigned int pin, unsigned int to) :
  Relay(id, to), timeout(NULL), pin(pin) {
  setup();
}

GPIO_Relay::~GPIO_Relay (void) {
  if (timeout != NULL) {
    timeout->dispose();
  }
}

void GPIO_Relay::setup (void) {
  if (timeout != NULL) {
    timeout->dispose();
  }
  if (max_timeout > 0) {
    timeout = new Timer(max_timeout, &GPIO_Relay::off, *this, true);
  }
  else {
    timeout = NULL;
  }
  pinMode(pin, OUTPUT);
  digitalWrite(pin, LOW);
}

void GPIO_Relay::on (void) {
  count += 1;
  if (state == 0) {
    state = 1;
    digitalWrite(pin, HIGH);
  }
  if (max_timeout > 0) {
    timeout->start();
  }
}

void GPIO_Relay::off(void) {
  if (state == 1) {
    state = 0;
    digitalWrite(pin, LOW);
  }
  if (max_timeout > 0) {
    timeout->stop();
  }
}

void GPIO_Relay::set_timeout(unsigned int t) {
  if (t == 0) {
    if (timeout != NULL) {
      timeout->dispose();
      timeout = NULL;
    }
  }
  else {
    if (timeout != NULL) {
      timeout->changePeriod(t);
    }
    else {
      timeout = new Timer(t, &GPIO_Relay::off, *this, true);
    }
  }
  max_timeout = t;
}

I2C_Relay::I2C_Relay(int id, MCP23008_I2C *controller) :
  Relay(id), timeout(NULL), controller(controller) {
    setup();
  }

I2C_Relay::I2C_Relay(int id, unsigned int to, MCP23008_I2C *controller) :
  Relay(id, to), timeout(NULL), controller(controller) {
    setup();
  }

I2C_Relay::~I2C_Relay(void) {
  if (timeout != NULL) {
    timeout->dispose();
  }
}

void I2C_Relay::on(void) {
  controller->set_pin(id, 1);
  if (controller->status == 0) {
    state = 1;
    if (max_timeout > 0) {
      timeout->start();
    }
  }
  count += 1;
}

void I2C_Relay::off(void) {
  controller->set_pin(id, 0);
  if (controller->status == 0) {
    state = 0;
    if (max_timeout > 0) {
      timeout->stop();
    }
  }
}

void I2C_Relay::setup (void) {
  if (timeout != NULL) {
    timeout->dispose();
  }
  if (max_timeout > 0) {
    timeout = new Timer(max_timeout, &I2C_Relay::off, *this, true);
  }
  else {
    timeout = NULL;
  }
}

void I2C_Relay::set_timeout(unsigned int t) {
  if (t == 0) {
    if (timeout != NULL) {
      timeout->dispose();
      timeout = NULL;
    }
  }
  else {
    if (timeout != NULL) {
      timeout->changePeriod(t);
    }
    else {
      timeout = new Timer(t, &I2C_Relay::off, *this, true);
    }
  }
  max_timeout = t;
}

MCP23008_I2C::MCP23008_I2C(int offset) {
  this->address = base_address | offset;
  this->status = 0;
  this->relay_states = 0;
  init();
}

MCP23008_I2C::~MCP23008_I2C(void) {
  clear();
}

void MCP23008_I2C::init(void) {
  // if Wire is already intialized skip
  if (!Wire.isEnabled()) {
    Wire.begin();
  }
  // set all pins for output
  Wire.beginTransmission(address);
  Wire.write(IODIR);
  Wire.write(0x00);
  status = Wire.endTransmission();
  // disable pull-up resistors
  Wire.beginTransmission(address);
  Wire.write(GPPU);
  Wire.write(0x00);
  status = Wire.endTransmission();
  // set all pins to off
  clear();
}

int MCP23008_I2C::set_pin(int id, int state) {
  byte new_states = relay_states;
  byte bit_pos = 0;

  // get bit position we need to set/clear
  switch(id) {
    case 1:
      bit_pos = 0x01;
      break;
    case 2:
      bit_pos = 0x02;
      break;
    case 3:
      bit_pos = 0x04;
      break;
    case 4:
      bit_pos = 0x08;
      break;
    case 5:
      bit_pos = 0x10;
      break;
    case 6:
      bit_pos = 0x20;
      break;
    case 7:
      bit_pos = 0x40;
      break;
    case 8:
      bit_pos = 0x80;
    default:
      return(-1); // bad input so just return
  }

  // set new states
  if (state == 0) {
    new_states &= ~bit_pos; // turn off bit
  }
  else if (state == 1) { // turn on bit
    new_states |= bit_pos;
  }
  else {
    return -1; // bad input
  }

  Wire.beginTransmission(address);
  Wire.write(OLAT);
  Wire.write(new_states);
  status = Wire.endTransmission();

  // update only on success
  if (status == 0) {
    relay_states = new_states;
  }

  return status;
}

void MCP23008_I2C::clear(void) {
  // set all pins to off
  Wire.beginTransmission(address);
  Wire.write(OLAT);
  Wire.write(0x00);
  status = Wire.endTransmission();
  relay_states = 0;
}
