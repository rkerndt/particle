/* Rickie Kerndt <rkerndt@cs.uoregon.edu
 * relay.h
 * Relay control with timeout
*/
#pragma once
#include <Particle.h>

/* Base class for relays, specifies the desired interface for operating and
checking the status of a relay. Each derived class must implement a timeout
mechanism such that no relay can remain in an 'on' state exceeding the
max_timeout value unless a subsequent 'on' method call is received. Any call to
'on' method to a relay already in the 'on' state will reset the timeout for
for that relay. */
class Relay {
protected:
  unsigned int max_timeout; // timeout in milliseconds
  int id;    // unique identifier
  int count; // cummulative calls to on()
  int state; // 1-> on, 0-> off

public:
  Relay(int id);
  Relay(int id, unsigned int max_timeout);
  virtual ~Relay(void) {}

  /* Unimplemented (pure virutal) methods. Each derived class must implement these two*/
  virtual void on(void)=0;
  virtual void off(void)=0;

  /* Implemented virtual methods. set_timeout() should be overridden to
  handle resetting the value used for relay 'on' timeout with the timeout
  solution implemented in the derived class */
  virtual unsigned int get_timeout(void) const;
  virtual void set_timeout(unsigned int ms);
  virtual int get_count(void) const;
  virtual int get_state(void) const;
  virtual int get_id(void) const;
};

/* GPIO digital write based relay controls. Uses the particle Timer class to
implement the relay timeout feature. Adds a pin attribute which is the gpio pin
connected to the signal input of the relay */
class GPIO_Relay : public Relay {
  Timer *timeout;
  int pin;

public:
  GPIO_Relay (int id, unsigned int pin);
  GPIO_Relay (int id, unsigned int pin, unsigned int to);
  ~GPIO_Relay (void);

  void on(void);
  void off(void);
  void set_timeout(unsigned int ms);

private:
  void setup();
};

class MCP23008_I2C {

  // register addresses
  // configuration taken from MCP23008 Product Brochure found at
  // one time here http://ww1.microchip.com/downloads/en/DeviceDoc/21919e.pdf
  const byte IODIR = 0x00; // I/O direction
  const byte IPOL = 0x01;  // input polarity port
  const byte GPINTEN = 0x02; // interrupt on change pins
  const byte DEFVAL = 0x03; // default value
  const byte INTCON = 0x04; // interrupt on change control
  const byte GPPU = 0x06; // GPIO pull-up resistor
  const byte INTF = 0x07; // interrupt flag register
  const byte INTCAP = 0x08; // interrupt captured value for port register
  const byte GPIO = 0x09; // general purpose I/O port
  const byte OLAT = 0x0A; // output latch register

  // configuration
  const byte base_address = 0x20; // base i2c address for MCP23008 device
  int address;      // base | offset, offset provided in constructor
  byte relay_states;

public:
  byte status;
  MCP23008_I2C(int offset);
  ~MCP23008_I2C(void);

  void setup(void);
  int set_pin(int id, int state);

private:
  void init(void);
  void clear(void);
};

class I2C_Relay : public Relay {
  Timer *timeout;
  MCP23008_I2C *controller;

public:
  I2C_Relay(int id, MCP23008_I2C *controller);
  I2C_Relay(int id, unsigned int to, MCP23008_I2C *controller);
  ~I2C_Relay(void);

  void on(void);
  void off(void);
  void set_timeout(unsigned int);

private:
  void setup(void);
};
